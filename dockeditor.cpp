#include "dockeditor.h"

DockEditor::DockEditor()
{
    dock = new QDockWidget;
    editor = new TextEditor;
    dock->setWidget(editor);
}

DockEditor::~DockEditor()
{
    delete editor;
    delete dock;
}

void DockEditor::SetDockTitle()
{
    dock->setWindowTitle(editor->getFileName());
}





