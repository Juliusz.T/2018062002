/**
  * @file dockeditor.h
  * @author Johan Thelin <e8johan@gmail.com>
  * @date September 2014
  * @brief STLView class definition.
  *
  * STLView class is responsible for displaying making a 3D scene,
  * displaying STLs, setting and managing camera movement.
  */

#ifndef STLVIEW_H
#define STLVIEW_H

#include <QGLWidget>
#include <QFile>

/**
 * @brief The STLView class - manages the 3D scene and objects on it.
 */
class STLView : public QGLWidget
{
    Q_OBJECT
public:
    /**
     * @brief STLView - constructor.
     * @param[in] localization of STL file
     * @param[in] parent - zero by default
     *
     * Initialize the whole 3D displaying:
     * boundaries of scale, rotations, translations of STL,
     * dislpays STL file maually vertex by vertex.
     */
    STLView(const QString &localization, QWidget *parent = 0);

protected:
    /**
     * @Initializes graphics.
     *
     * Initializes graphic engine, set lights and colors.
     */
    void initializeGL();
    /**
     * @brief Scale view to STL size and pozition.
     * @param[in] width of model
     * @param[in] height of model
     */
    void resizeGL(int, int);
    /**
     * @brief Changes scene colours and camera orientation.
     */
    void paintGL();

    /**
     * @brief Handles mouse buttons actions
     * @param[in] QMouseEvent
     */
    void mousePressEvent(QMouseEvent *);
    /**
     * @brief Handles mouse buttons release action.
     * @param[in] QMouseEvent
     */
    void mouseReleaseEvent(QMouseEvent *);
    /**
     * @brief Handles mouse movement events
     * @param[in] QMouseEvent
     */
    void mouseMoveEvent(QMouseEvent *);

    /**
     * @brief Handles mouse wheel actions
     */
    void wheelEvent(QWheelEvent *);

private:
    /**
     * @brief Simple strict that holds one STL vertex - set of 3 numbers.
     */
    struct StlVertex {
        qreal x, y, z;
    };

    typedef QList<StlVertex> StlVertexList;/**< \brief List of Stlvertex that makes the whole STL 3D object*/

    /**
     * @brief Representation of the STL facet – information about orientation and vertex.
     */
    struct StlFacet {
        StlVertex normal;
        StlVertexList vertexes;
    };

    typedef QList<StlFacet> StlFacetList;

    struct StlSolid {
        QString name;
        StlFacetList facets;
    };

    typedef QList<StlSolid> StlSolidList;

    StlVertex parseVertex(const QString &line, bool *ok);

    StlSolidList m_solids;
    qreal m_rotx, m_roty, m_rotz;
    qreal m_offsetX, m_offsetY, m_offsetZ;
    qreal m_scale;

    qreal m_deltaRotX, m_deltaRotY;
    qreal m_deltaPanX, m_deltaPanY;

    bool m_isRotating;
    bool m_isPanning;

    QPoint m_startPoint;
};

#endif // STLVIEW_H
