/**
  * @file dockeditor.h
  * @author Juliusz Stanislaw Tarnowski
  * @date June 2018
  * @brief DockEditor class definition
  */

#ifndef DOCKEDITOR_H
#define DOCKEDITOR_H

#include "texteditor.h"
#include <QDockWidget>

/**
 * @brief The DockEditor class - holds the text editor and container for docking.
 */
class DockEditor
{
public:
    /**
     * @brief DockEditor - constructor.
     *
     * Creates new TextEditor and QDockWidget objects, bound them together.
     */
    DockEditor();

    /**
     * @brief DockEditor - destructor.
     *
     * Delets TextEditor and QDockWidget objects.
     */
    ~DockEditor();

    TextEditor *editor; /**< \brief Text Editor object */
    QDockWidget *dock; /**< \brief Container for docking a editor */

    /**
     * @brief Sets name of currently edited file as a title of TextEditor window.
     */
    void SetDockTitle();
};

#endif // DOCKEDITOR_H
