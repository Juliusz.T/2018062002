/**
  * @file dockeditor.h
  * @author Juliusz Stanislaw Tarnowski
  * @date June 2018
  * @brief DockEditor class definition
  */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "stlview.h"
#include "dockeditor.h"

#include <QMainWindow>
#include <QPushButton>
#include <QFile>
#include <QMenu>

#include <QList>

/**
 * @brief The MainWindow class - holds aplication together.
 */
class MainWindow : public QMainWindow
{
    Q_OBJECT

public slots:

    /**
     * @brief Adds new editor window, docked under the previous one.
     *
     * Creates new editor, adds it to the dockEditors list and
     * sets correspondent signals.
     */
    void appendNextEditor();

    /**
     * @brief Compiles OpenSCAD code to STL and regenerate new scene with fresh STL.
     * @param[in] OpenScadFile - forwarded to the file with OpenSCAD source
     */
    void compileOpenSCADCode(QString OpenScadFile);

    /**
     * @brief Reload name of dockEditor, when editor calls it.
     */
    void reloadName();

public:

    /**
     * @brief Simple constructor - initialize whole application.
     * @param[in] parent - zero by default
     *
     * Initialize the whole applicaion with new, clean editor, empty scene3d,
     * toolbar and terminal.
     */
    explicit MainWindow(QWidget *parent = 0);

    /**
     * @brief Empty destructor.
     */
    ~MainWindow();

    /**
     * @brief Compile text file to STL file that can be shown by STLView class.
     * @param[in] STLLocalization - directory to file with 3d model in .stl format.
     *
     * Function used console command to compile text file with OpenScad
     * commands. It compiles file under original name and in original localization.
     */
    void reloadSTL(QString STLLocalization);

private:
//    QMenu *menu; /**< \brief briefbrief */
    STLView *scene3d; /**< \brief Holds a STLView object - a widget that dislpays STL in 3D. */

    QList<DockEditor *> dockEditors; /**< \brief List of currently open text editors objects.*/

};

#endif // MAINWINDOW_H
