#include "mainwindow.h"
#include "texteditor.h"
#include "stlview.h"
#include <QProcess>
#include <QStringList>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    this->setWindowTitle("STL Editor");

    QString localization = "/home/juliusz/git/STL/logo.stl";
    scene3d = new STLView(localization);
    setCentralWidget(scene3d);

    appendNextEditor();
}

MainWindow::~MainWindow()
{
}

void MainWindow::appendNextEditor()
{
    dockEditors.append(new DockEditor);
    dockEditors.last()->dock->setAllowedAreas(Qt::LeftDockWidgetArea);
    addDockWidget(Qt::LeftDockWidgetArea, dockEditors.last()->dock);
    connect(dockEditors.last()->editor, SIGNAL(sigNewEditor()), this, SLOT( appendNextEditor()) );
    connect(dockEditors.last()->editor, SIGNAL(sigCompileOpenSCADCode(QString)), this, SLOT(compileOpenSCADCode(QString)));
    connect(dockEditors.last()->editor, SIGNAL(sigNewName()), this, SLOT(reloadName()));
}

void MainWindow::reloadSTL(QString STLLocalization)
{
    this->centralWidget()->close();
    delete scene3d;
    scene3d = new STLView(STLLocalization);
    setCentralWidget(scene3d);
}

void MainWindow::compileOpenSCADCode(QString OpenScadFile)
{
    QStringList pieces = OpenScadFile.split( "/" );
    QString stlName = pieces.value( pieces.length() - 1 );
    pieces = stlName.split( "." );
    stlName = pieces.value( pieces.length() - 2 );
    this->setWindowTitle(stlName);
    QString localization = "/home/juliusz/git/STL/" + stlName + ".stl";
    QString app = "openscad -o";
    QString command = (app + " " + localization);
    command = command + " " + OpenScadFile;

    QProcess process;
    process.start(command);
    process.setProcessChannelMode(QProcess::ForwardedChannels);
    process.waitForFinished();

    reloadSTL(localization);
}

void MainWindow::reloadName()
{
    for( int i=0; i<dockEditors.count(); ++i )
    {
        dockEditors[i]->SetDockTitle();
    }
}
