#include "texteditor.h"

TextEditor::TextEditor()
{
    isUntitled = true;
    curFile = "";
    init();
}

TextEditor::TextEditor(const QString &filename)
{
    isUntitled = false;
    curFile = filename;
    init();
    plotCurentFile();
}

QString TextEditor::getFileName()
{
    QStringList pieces = curFile.split( "/" );
    QString result = pieces.value( pieces.length() - 1 );
    return result;
}

void TextEditor::init()
{    
    layout = new QBoxLayout(QBoxLayout::TopToBottom, this);

    fileToolBar = new QToolBar;
    editor = new QTextEdit;

    layout->addWidget(fileToolBar);
    layout->addWidget(editor);
    setLayout(layout);

    createActions();
    createToolBars();
}

void TextEditor::plotCurentFile()
{
    QFile file(curFile);
    if (!file.open(QFile::ReadOnly | QFile::Text))
    {
        QMessageBox::warning(this, tr("SDI"),
                             tr("Cannot read file %1:\n%2.")
                             .arg(curFile)
                             .arg(file.errorString()));
        return;
    }

    if(isUntitled == false)
    {
        QTextStream streamFromFile(&file);
        QApplication::setOverrideCursor(Qt::WaitCursor);
        editor->setPlainText(streamFromFile.readAll());
        QApplication::restoreOverrideCursor();
    }
}

bool TextEditor::maybySave()
{
    if (editor->document()->isModified())
    {
        QMessageBox::StandardButton ret;
        ret = QMessageBox::warning(this, tr("SDI"),
                     tr("The document has been modified.\n"
                        "Do you want to save your changes?"),
                     QMessageBox::Save | QMessageBox::Discard
                     | QMessageBox::Cancel);
        if (ret == QMessageBox::Save)
            return saveFile();
        else if (ret == QMessageBox::Cancel)
            return false;
    }
    return false;
}

bool TextEditor::saveFile()
{
    if (isUntitled) {
        QString temp = QFileDialog::getSaveFileName(this, "Save file as...");
        if(temp!=""){
            curFile = temp;
        }
    }

    QFile file(curFile);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox::warning(this, tr("SDI"),
                             tr("Cannot write file %1:\n%2.")
                             .arg(curFile)
                             .arg(file.errorString()));
        return false;
    }

    QTextStream out(&file);
    QApplication::setOverrideCursor(Qt::WaitCursor);
    out << editor->toPlainText();
    QApplication::restoreOverrideCursor();

    return true;
}

void TextEditor::createActions()
{
    newAct = new QAction(QIcon(":/images/new.png"), tr("&New"), this);
    newAct->setShortcuts(QKeySequence::New);
    newAct->setStatusTip(tr("Create a new file"));
    connect(newAct, SIGNAL(triggered()), this, SLOT(newFile()));

    openAct = new QAction(QIcon(":/images/open.png"), tr("&Open..."), this);
    openAct->setShortcuts(QKeySequence::Open);
    openAct->setStatusTip(tr("Open an existing file"));
    connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

    saveAct = new QAction(QIcon(":/images/save.png"), tr("&Save"), this);
    saveAct->setShortcuts(QKeySequence::Save);
    saveAct->setStatusTip(tr("Save the document to disk"));
    connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

    saveAsAct = new QAction(QIcon(":/images/save.png"), tr("Save &As..."), this);
    saveAsAct->setShortcuts(QKeySequence::SaveAs);
    saveAsAct->setStatusTip(tr("Save the document under a new name"));
    connect(saveAsAct, SIGNAL(triggered()), this, SLOT(saveAs()));


    undoAct = new QAction(QIcon(":/images/undo.png"), tr(" "), this);
    undoAct->setShortcuts(QKeySequence::Undo);
    undoAct->setStatusTip(tr(" "));
    connect(undoAct, SIGNAL(triggered()), this, SLOT(undo()));

    redoAct = new QAction(QIcon(":/images/redo.png"), tr("&Redo"), this);
    redoAct->setShortcuts(QKeySequence::Redo);
    redoAct->setStatusTip(tr(" "));
    connect(redoAct, SIGNAL(triggered()), this, SLOT(redo()));

    cutAct = new QAction(QIcon(":/images/cut.png"), tr("&Cut"), this);
    cutAct->setShortcuts(QKeySequence::Cut);
    cutAct->setStatusTip(tr(" "));
    connect(cutAct, SIGNAL(triggered()), this, SLOT(cut()));

    copyAct = new QAction(QIcon(":/images/copy.png"), tr("&Copy"), this);
    copyAct->setShortcuts(QKeySequence::Copy);
    copyAct->setStatusTip(tr(" "));
    connect(copyAct, SIGNAL(triggered()), this, SLOT(copy()));

    pasteAct = new QAction(QIcon(":/images/paste.png"), tr("&Paste"), this);
    pasteAct->setShortcuts(QKeySequence::Paste);
    pasteAct->setStatusTip(tr(" "));
    connect(pasteAct, SIGNAL(triggered()), this, SLOT(paste()));

    compileOpenSCADCodeAct = new QAction(QIcon(":/images/close.png"), tr("&Compile"), this);
    compileOpenSCADCodeAct->setShortcuts(QKeySequence::Replace);
    compileOpenSCADCodeAct->setStatusTip(tr(" "));
    connect(compileOpenSCADCodeAct, SIGNAL(triggered()), this, SLOT(compileOpenSCADCode()));

}

void TextEditor::createToolBars()
{
    fileToolBar->addAction(newAct);
    fileToolBar->addAction(openAct);
    fileToolBar->addAction(saveAct);
    fileToolBar->addAction(saveAsAct);
    fileToolBar->addSeparator();

    fileToolBar->addAction(undoAct);
    fileToolBar->addAction(redoAct);
    fileToolBar->addAction(cutAct);
    fileToolBar->addAction(copyAct);
    fileToolBar->addAction(pasteAct);
    fileToolBar->addSeparator();
    fileToolBar->addAction(compileOpenSCADCodeAct);
}

void TextEditor::newFile()
{
    emit sigNewEditor();
}

void TextEditor::open()
{
    maybySave();
    QString temp = QFileDialog::getOpenFileName(this, "Open file...");
    if(temp!=""){
        curFile = temp;
        isUntitled = false;
    }
    emit(sigNewName());
    plotCurentFile();
}

bool TextEditor::save()
{
    if(isUntitled == true) return saveAs();
    else
    {
        saveFile();
        return true;
    }
}

bool TextEditor::saveAs()
{
    QString temp = QFileDialog::getSaveFileName(this, "Save file as...");
    if(temp!=""){
        curFile = temp;
        isUntitled = false;
    }
    saveFile();
    emit(sigNewName());
    return true;
}

void TextEditor::undo()
{
    editor->undo();
}

void TextEditor::redo()
{
    editor->redo();
}

void TextEditor::cut()
{
    editor->cut();
}

void TextEditor::copy()
{
    editor->copy();
}

void TextEditor::paste()
{
    editor->paste();
}

void TextEditor::compileOpenSCADCode()
{
    save();
    emit(sigNewName());
    emit(sigCompileOpenSCADCode(curFile));
}
