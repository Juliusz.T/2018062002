/**
  * @file dockeditor.h
  * @author Juliusz Stanislaw Tarnowski
  * @date June 2018
  * @brief DockEditor class definition
  */

#ifndef TEXTEDITOR_H
#define TEXTEDITOR_H

#include <QObject>
#include <QApplication>
#include <QWidget>
#include <QIcon>
#include <QAction>
#include <QTextEdit>
#include <QTextStream>
#include <QFileDialog>
#include <QMessageBox>
#include <QToolBar>
#include <QBoxLayout>
#include <QCloseEvent>

/**
 * @brief The TextEditor class - manages the text editor.
 *
 * Holds all of it's key features like saving, loading the file and
 * reloading STLs
 */
class TextEditor: public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief TextEditor - simplest constructor.
     *
     * Starts empty, untitled file to edit.
     */
    TextEditor();

    /**
     * @brief TextEditor - constructor.
     * @param[in] filename - name of text file to load.
     *
     * Starts editor with file specified by it's directory and
     * shows it at scene3D.
     */
    TextEditor(const QString &filename);

    QBoxLayout *layout; /**< \brief Defines layout of text editor part*/
    QTextEdit *editor; /**< \brief Editor object */
    QToolBar *fileToolBar; /**< \brief Upper ToolBar object*/
    QString curFile; /**< \brief Directory of currently open file */
    bool isUntitled; /**< \brief Information about whether currently open file was already saved.*/

    /**
     * @brief Extracts file name from it's full directory.
     * @param[out] Raw file name
     */
    QString getFileName();

signals:
    /**
     * @brief Signal emited when new text edtor is open.
     */
    void sigNewEditor();

    /**
     * @brief Signal emited when user will compile code and show resulting .stl file.
     * @param[in] Directory of file.
     */
    void sigCompileOpenSCADCode(QString OpenScadFile);

    /**
     * @brief Signal emited when the file in the editor wants to refresh name.
     * @param[in] Directory of file.
     */
    void sigNewName();

private:
    /**
     * @brief Initialize the TextEditor part of application,
     * set all of key features.
     *
     * Creates new layout of TextEditor part -
     * including text editor and tool bar,
     * initialize creation of Action and ToolBar.
     */
    void init();
    /**
     * @brief Dislpays text file on the text editor.
     *
     * Functions tries to open textfile, returns warning if fails.
     * If succeeds, currently displayed text is replaced with text from
     * file.
     */
    void plotCurentFile();
    /**
     * @brief Ask user whether to save the file that is currently closing.
     * @param[out] Logic information about whether
     * saving or not.
     */
    bool maybySave();
    /**
     * @brief Saving currently open file.
     * @param[out] True if saved correctly.
     *
     * Asks user for a file name and directory if wasn't saved before.
     */
    bool saveFile();

    /**
     * @brief Defines QAction, connect them with proper functions.
     *
     * For all QActions members of TextEditor the proper connections
     * with functions are defined. Actions are linked with images
     * (if they are ment to be toolbar buttons), are given status bar info
     * and keyboard shortcut.
     */
    void createActions();
    /**
     * @brief Creates a toolbar for text editor.
     *
     * Creates a toolbar and adds all of the buttons,
     * connects button with defined QActions.
     */
    void createToolBars();

    QAction *newAct; /**< \brief Action of creating a new editor */
    QAction *openAct; /**< \brief Action of opening existing file */
    QAction *saveAct; /**< \brief Action of saving a file */
    QAction *saveAsAct; /**< \brief Action of saving file as something else */

    QAction *undoAct; /**< \brief Action of undoing a change */
    QAction *redoAct; /**< \brief Action of redoing a undone change */
    QAction *cutAct; /**< \brief Action of cutting a marked text */
    QAction *copyAct; /**< \brief Action of copying a market text */
    QAction *pasteAct; /**< \brief Action of pasting previously cut/copied text */

    QAction *compileOpenSCADCodeAct; /**< \brief Action of compiling code and show STL on scene */

private slots:
    void newFile();
    void open();
    bool save();
    bool saveAs();

    void undo();
    void redo();
    void cut();
    void copy();
    void paste();

    void compileOpenSCADCode();
};

#endif // TEXTEDITOR_H
